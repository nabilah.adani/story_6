from django import forms
from .models import form_stat

class FormStatus(forms.Form):
	nama = forms.CharField(label = 'Nama', max_length = 40, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your Name', 'id' : 'id_name'}), required = True)
	deskripsi = forms.CharField(label = 'Status', max_length = 300, widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your Status', 'id' : 'id_status'}), required = True)