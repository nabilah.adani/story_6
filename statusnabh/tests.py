from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import form_stat
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story6_using_profile_html(self):
        response = Client().get('/profil/')
        self.assertTemplateUsed(response, 'profil.html')
    
    def test_profil_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_hasil_url_is_exist(self):
        response = Client().get('/hasil/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_status(self):
        new_status = form_stat.objects.create(nama = 'Nabilah Adani', deskripsi = 'Sedang pusing dengan tugas PPW, POK dan SDA')
        counting_all_available_activity = form_stat.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)
    
    def test_landing_page_is_completed(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hallo! Apa Kabar?", html_response)

class story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        nama = selenium.find_element_by_id('id_name')
        description = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_class_name('button-submit')

        # Fill the form with data
        nama.send_keys('Nabilah Adani')
        description.send_keys('Sedang memikirkan bagaimana bisa lulus PPW')

        # submitting the form
        submit.send_keys(Keys.RETURN)




    
    
