from django.urls import path
from .views import index, profil, addForm

urlpatterns = [
    path('', index, name='index'),
    path('profil/', profil, name='profil'),
    path('status/', index, name='status'),
    path('hasil/', addForm, name='hasil'),
]