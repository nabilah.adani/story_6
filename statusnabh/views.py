from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .forms import FormStatus
from .models import form_stat
# Create your views here.

response = {'author' : 'Nabilah Adani Nurulizzah'}
def profil(request):
    return render(request, 'profil.html')

def index(request):
    form = FormStatus()
    status = form_stat.objects.all()
    if 'DeleteAll' in request.POST :
		        form_stat.objects.all().delete()
		        context = {
                    'form' : form,
                    'status' : status
                }
		        return render(request, 'list_status.html', context)	
    context = {
            'form' : form,
            'status' : status
        }
    # store all objects
    return render(request, 'list_status.html', context)
    
    
def addForm(request):
    form = FormStatus(request.POST or None)
    status = form_stat.objects.all()
    if request.method == "POST":
        response['nama'] = request.POST['nama']
        response['deskripsi'] = request.POST['deskripsi']
        a = form_stat(nama = response['nama'], deskripsi = response['deskripsi'])
        a.save()
        form = FormStatus()
        context = {
            'form' : form,
            'status' : status
        }
        return redirect('index')
    else: 
        form = FormStatus()
        context = {
            'form' : form,
            'status' : status
        }
        return render(request, 'list_status.html', context)

